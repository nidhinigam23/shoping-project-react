import React, { Component } from 'react';
import './App.css';
import Navbar from './components/Navbar'
import Home from './components/Home';
import CartsItem from './components/cartsItem'
class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      show: true,
      carts: [],
      length: 0,
      totalPrice: 0,
      amount: 1,
    }
  }

  removeItem = (id) => {
    const newCarts = this.state.carts.filter((item) => {
      return item.id !== id;
    })
    let length = newCarts.length;
    this.setState({ carts: newCarts, length: length });
    let ans = 0;
    newCarts.map((item) => (ans += item.amount * item.price))
    this.setState({ carts: newCarts, length: length, totalPrice: ans });
  }

  handlePrice = () => {
    let ans = 0;
    this.state.carts.map((item) => (ans += item.amount * item.price))
    this.setState({ totalPrice: ans });
  }

  incrementAmount = (id, d) => {
    let ans = 0;
    this.state.carts.map((item) => {
      if (item.id === id) {
        item.amount += d;
        ans = d * item.price;

      }
    })
    this.setState({ totalPrice: this.state.totalPrice + ans })
  }
  decrementAmount = (id, d) => {
    let ans = 0;
    this.state.carts.map((item) => {
      if (item.id === id && item.amount > 1) {
        item.amount--;
        ans = 1 * item.price;
      }
    })
    this.setState({ totalPrice: this.state.totalPrice - ans })

  }

  handleClick = (item) => {
    if (this.state.carts.indexOf(item) !== -1) {
      return;
    }
    else {
      this.state.carts.push(item);
      console.log(this.state.carts);
      let length = this.state.carts.length;
      this.setState({ length: length });
    }
  }
  componentDidMount() {
    this.removeItem();
  }

  showHandler = (value) => {
    this.setState({ show: value })
  }
  render() {
    const { show, length, carts } = this.state;
    return (
      <div>
        <Navbar showHandler={this.showHandler} length={length} />
        {show ? <Home handleClick={this.handleClick} /> : <CartsItem carts={carts} removeItem={this.removeItem} totalPrice={this.state.totalPrice} handlePrice={this.handlePrice} amount={this.state.amount} incrementAmount={this.incrementAmount} decrementAmount={this.decrementAmount} />}
      </div>
    )
  }
}

export default App
