import React, { Component } from 'react'
import Card from './Cards';
import list from '../data';
import './style/Home.css';
class amazon extends Component {
    render() {
        return (
            <>
                <div className='card-container'>
                    {
                        list.map((item) => {
                            return (

                                <Card key={item.id} item={item} handleClick={this.props.handleClick} />

                            )
                        })
                    }
                </div>
            </>

        )
    }
}

export default amazon
