import React, { Component } from 'react'
import './style/cards.css';
class Cards extends Component {
    render() {
        const { title, author, price, img, amount } = this.props.item;
        const { handleClick } = this.props;
        return (
            <div className="cards">
                <div className="image_box">
                    <img src={img} alt="" />
                </div>
                <div className="details">
                    <p id='title'>{title}</p>
                    <p><span>Author:</span>{author}</p>
                    <p><span>Price:</span>{price}</p>
                    <button onClick={() => handleClick(this.props.item)}>Add to Cart</button>
                </div>
            </div>
        )
    }
}

export default Cards

